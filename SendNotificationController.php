<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;


class SendNotificationController extends Controller {

public function SendMessage(Request $request) {
		$name = $request->input('name');
		$phone = $request->input('phone');
		$request_id = $request->input('tranid');
		$form_id = $request->input('formid');

		$post_content = print_r($_POST, true);
		file_put_contents('post.txt', $post_content, FILE_APPEND);

		$token = '$2y$10$YdYcL/rpDMYxU7wzO46Cc.gvamIA4/j.vZT691iOTWG7sJPlCHIQC';

		$pattern = '/[^0-9]/';
		$phone = preg_replace($pattern, '', $phone); // убираем все символы, оставляем только цифры

		switch ( substr($phone, 0, 1) ) {
    		case '0': // если номер в формате 0509002010
        		if (strlen($phone) == 10) {
        			$phone = '38' . $phone; // добавляем 38
        		}
        	break;
    		case '8': // если номер в формате 80509002010
        		if ( (substr($phone, 0, 2) == '80') && (strlen($phone) == 11) ) {
        			$phone = '3' . $phone; // добавляем 3
        		}
        	break;
    		default: // если номер в формате 509002010
        		if (strlen($phone) == 9) {
        			$phone = '380' . $phone; // добавляем 380
        		}
        	break;
		}

		file_put_contents('post.txt', $phone, FILE_APPEND);
		$json = '
		{
			"name": "request_data",
			"recipients": "' . $phone . '",
			"sender": "SALES only"
		';
		if ( setting('site.message_text') ) {
			$json .=
			',
			"message": "' . addslashes(setting('site.message_text')) . '"
			';
		}

		if ( setting('site.button_name') && setting('site.button_url') ) {
			$json .=
			',
			"button_name": "' . setting('site.button_name') . '",
			"button_url" : "' . setting('site.button_url') . '"
			';
		}

		if ( setting('site.image') ) {
			$json .=
			',
			"url_image": "' . url('storage/' . setting('site.image') ) . '"
			';
		}

		$json .= '
		}
		';

		$pattern = '/[\r\n]+/';

		$json = preg_replace($pattern, ' ', $json);
		$curl = curl_init();
		$options = array(CURLOPT_URL => 'https://my2.viber.net.ua/api/v2/viber/dispatch',
                 		 CURLOPT_HEADER => false,
                 		 CURLOPT_RETURNTRANSFER => True,
                 		 CURLOPT_CUSTOMREQUEST => "POST",
                 		 CURLOPT_HTTPHEADER => array("content-type: application/json",
                 		 							 "Accept-Charset: utf-8",
                									 "Authorization: Bearer " . $token,
                									 ),
                		CURLOPT_POSTFIELDS => $json,
                		);
		curl_setopt_array($curl, $options);
		$response = curl_exec($curl);
		$err = curl_error($curl);
		curl_close($curl);

		if ($err) {
      		return $err;
    	} else {
    		//return $response;
    		return $json . '<br><br>' . $response . '<br><br>' . storage_path( setting('site.image') ) . '<br><br>' . url('storage/' . setting('site.image') );

    	}
	}

}

?>